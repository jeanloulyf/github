import Ember from 'ember';
import LocalStorage from 'github/utils/local-storage';

export default Ember.Object.extend({
  ajax: Ember.inject.service(),
  store: Ember.inject.service(),
  open(authorization) {
    return this.get('ajax').request(`http://localhost:9999/authenticate/${authorization.authorizationCode}`).then((data) => {
      return this._fetchAuthenticatedUser(data.token);
    });
  },
  fetch() {
    let token = LocalStorage.fetch('github-token');
    if (token) {
      return this._fetchAuthenticatedUser(token);
    } else {
      return Ember.RSVP.reject();
    }
  },
  close() {
    LocalStorage.remove('github-token');
  },
  _fetchAuthenticatedUser(token) {
    let headers = {
      'Authorization': `token ${token}`
    };
    return this.get('ajax').request('https://api.github.com/user', {headers}).then((user) => {
      this.get('store').pushPayload({ users: [user] });
      LocalStorage.save('github-token', token);
      return {
        token: token,
        currentUser: this.get('store').peekRecord('user', user.login)
      };
    });
  }
});
