import Ember from 'ember';

export default Ember.Route.extend({
  session: Ember.inject.service(),
  actions: {
    logInWithGithub() {
      this.get('session').open('github-oauth2').then(() => {
        this.transitionTo('dashboard');
      });
    },
    logOut() {
      this.get('session').close().then(() => {
        this.transitionTo('index');
      });
    },
    accessDenied() {
      this.transitionTo('index');
    }
  }
});
