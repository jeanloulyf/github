import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  primaryKey: 'login',
  normalize(modelClass, responseHash, prop) {
    responseHash.links = {
      repos: responseHash.repos_url
    };

    return this._super(modelClass, responseHash, prop);
  }
});
