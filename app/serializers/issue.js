import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  primaryKey: 'number',

  normalize(modelClass, responseHash, prop) {
    responseHash.commentsCount = responseHash.comments;
    delete responseHash.comments;

    responseHash.links = {
      comments: `${responseHash.url}/comments`
    };

    return this._super(modelClass, responseHash, prop);
  }
});
