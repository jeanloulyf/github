import Ember from 'ember';

export default Ember.Component.extend({
	isEdited: Ember.computed('createdAt', 'updatedAt', function() {
		let created = this.get('createdAt');
		let updated =  this.get('updatedAt');

		if (created === updated) {
			return false
		} else {
			return true
		}
	})
});
